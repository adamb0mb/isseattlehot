import os
import random
import datetime
import json
from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.api import urlfetch
from xml.dom import minidom
from datetime import datetime
from datetime import date

class WeatherRecord(db.Model):
    temp_f = db.FloatProperty()
    location = db.StringProperty(multiline=True)
    station_id = db.StringProperty()
    wind_mph = db.FloatProperty()
    wind_dir = db.StringProperty()
    weather = db.StringProperty(multiline=True)
    timestamp = db.DateTimeProperty(auto_now_add=True)

class ForecastRecord(db.Model):
    forecastFor = db.DateTimeProperty(auto_now_add=False)
    temp_f = db.FloatProperty()
    location = db.StringProperty(multiline=True)
    cloudiness = db.FloatProperty()
    wind_mph = db.FloatProperty()
    wind_dir = db.FloatProperty()
    timestamp = db.DateTimeProperty(auto_now_add=True)

class MainPage(webapp.RequestHandler):
    def get(self):

        #redirect to the proper website
        if os.environ['SERVER_NAME'] == 'isseattlehot.appspot.com':
            self.redirect('http://www.isseattlehot.com')
        elif os.environ['SERVER_NAME'] == 'www.isseattlefreezing.com':
            self.redirect('http://www.isseattlehot.com/freezing')

        wr = db.GqlQuery("SELECT * FROM WeatherRecord ORDER BY timestamp DESC LIMIT 1")[0]
        judgement = ""
        judgementMessage = ""
        if wr.temp_f> 80:
            judgement = "Yes"
            judgementMessage = "Yes, Seattle is hot. It is currently " + str(wr.temp_f) + " degrees."
        else:
            judgement = "No"
            judgementMessage = "No, Seattle isn't hot. It is currently " + str(wr.temp_f) + " degrees."

        hostname = ""
        if int(os.environ['SERVER_PORT']) == 80:
            hostname = os.environ['SERVER_NAME']
        else:
            hostname = os.environ['SERVER_NAME']+":"+os.environ['SERVER_PORT']

        template_values = {
            'judgement': judgement,
            'judgementMessage': judgementMessage,
            'hostname': hostname
        }

        path = os.path.join(os.path.dirname(__file__),'index.django.html')
        self.response.out.write(template.render(path, template_values))

class FreezingPage(webapp.RequestHandler):
    def get(self):
        wr = db.GqlQuery("SELECT * FROM WeatherRecord ORDER BY timestamp DESC LIMIT 1")[0]
        judgement = ""
        judgementMessage = ""
        if wr.temp_f<= 32:
            judgement = "Yes"
            judgementMessage = "Yes, Seattle is friggin' freezing. It is currently " + str(wr.temp_f) + " degrees."
        else:
            judgement = "No"
            judgementMessage = "No, Seattle isn't freezing. It is currently " + str(wr.temp_f) + " degrees."

        hostname = ""
        if int(os.environ['SERVER_PORT']) == 80:
            hostname = os.environ['SERVER_NAME']
        else:
            hostname = os.environ['SERVER_NAME']+":"+os.environ['SERVER_PORT']

        template_values = {
            'judgement': judgement,
            'judgementMessage': judgementMessage,
            'hostname': hostname,
            'pageTitle': "Is Seattle Friggin' Freezing?"
        }

        path = os.path.join(os.path.dirname(__file__),'index.django.html')
        self.response.out.write(template.render(path, template_values))

class UpdateWeather(webapp.RequestHandler):
    def get(self):

        rand = random.randint(0,2000000000)
        WEATHER_URL = 'http://www.weather.gov/xml/current_obs/KBFI.xml?tick='+str(rand)
        WEATHER_NS = 'http://www.w3.org/2001/XMLSchema'
        result = urlfetch.fetch(WEATHER_URL)
        if result.status_code == 200:
            dom = minidom.parseString(result.content)
            wr = WeatherRecord()
            wr.temp_f = float(dom.getElementsByTagName('temp_f')[0].childNodes[0].data)
            wr.location = dom.getElementsByTagName('location')[0].childNodes[0].data
            wr.station_id = dom.getElementsByTagName('station_id')[0].childNodes[0].data
            wr.wind_mph = float(dom.getElementsByTagName('wind_mph')[0].childNodes[0].data)
            wr.wind_dir = dom.getElementsByTagName('wind_dir')[0].childNodes[0].data
            wr.weather = dom.getElementsByTagName('weather')[0].childNodes[0].data
            wr.put();
            self.response.headers['Content-Type'] = 'text/plain'
            self.response.out.write(wr.temp_f)
            self.response.out.write("<br />"+WEATHER_URL)

class UpdateForecast(webapp.RequestHandler):
    def get(self):

        rand = random.randint(0,2000000000)
        FORECAST_URL = 'http://www.wrh.noaa.gov/forecast/xml/xml.php?duration=48&interval=1&lat=47.6011125&lon=-122.3293984'
        FORECAST_NS = 'http://www.w3.org/2001/XMLSchema'
	result = urlfetch.fetch(FORECAST_URL)
        if result.status_code == 200:
            dom = minidom.parseString(result.content)
            fr = ForecastRecord()
            tomorrowForecast = dom.getElementsByTagName('forecastDay')[1]
            tomorrow6amForecast = tomorrowForecast.getElementsByTagName('period')[13]
            fr.temp_f = float(tomorrow6amForecast.getElementsByTagName('temperature')[0].childNodes[0].data)
            fr.cloudiness = float(tomorrow6amForecast.getElementsByTagName('skyCover')[0].childNodes[0].data)
            fr.wind_mph = float(tomorrow6amForecast.getElementsByTagName('windSpeed')[0].childNodes[0].data)
            fr.wind_dir = float(tomorrow6amForecast.getElementsByTagName('windDirection')[0].childNodes[0].data)
            fr.location = "Seattle"
            now = date.today()
            dateTimeString = tomorrowForecast.getElementsByTagName('validDate')[0].childNodes[0].data +" "+ str(now.year) + " 13:00:00"
            fr.forecastFor = datetime.strptime(dateTimeString, "%b %d %Y %H:%M:%S")
            fr.put()
            self.response.headers['Cont ent-Type'] = 'text/plain'
            self.response.out.write("Forecast For: ")
            self.response.out.write(fr.forecastFor)
            self.response.out.write(", Temperature: ")
            self.response.out.write(fr.temp_f)
            self.response.out.write(", cloudiness: ")
            self.response.out.write(fr.cloudiness)
            self.response.out.write(", wind direction: ")
            self.response.out.write(fr.wind_dir)
            self.response.out.write(", wind MPH: ")
            self.response.out.write(fr.wind_mph)

class GetForecast(webapp.RequestHandler):
    def get(self):
        fr = ForecastRecord()
        fr = db.GqlQuery("SELECT * FROM ForecastRecord ORDER BY forecastFor DESC LIMIT 1")[0]
	self.response.out.write(json.encode(fr))


class WeatherView(webapp.RequestHandler):
    def get(self):
        wr = WeatherRecord()
        wrs = db.GqlQuery("SELECT * FROM WeatherRecord ORDER BY timestamp DESC LIMIT 10")
        self.response.headers['Content-Type'] =  'text/HTML'
        self.response.out.write("Lastest Weather Records:<br />")
        self.response.out.write("<table border=1>")
        for wr in wrs:
            self.response.out.write("<tr>")
            self.response.out.write("<td>"+ wr.location +"</td>")
            self.response.out.write("<td>"+ wr.station_id +"</td>")
            self.response.out.write("<td>"+ str(wr.temp_f) +"</td>")
            self.response.out.write("<td>"+ str(wr.weather) +"</td>")
            self.response.out.write("<td>"+ str(wr.timestamp) +"</td>")
            self.response.out.write("</tr>")
        self.response.out.write("</table>")
        lastHot = db.GqlQuery("select * FROM WeatherRecord WHERE temp_f>=80 ORDER BY temp_f,timestamp DESC LIMIT 1")[0]
	self.response.out.write("This is broken: Last time it was 80 or warmer: " + str(lastHot.timestamp));

        fr = ForecastRecord()
        frs = db.GqlQuery("SELECT * FROM ForecastRecord ORDER BY timestamp DESC LIMIT 10")
        self.response.out.write("<br /><br />Lastest Weather Records:<br />")
        self.response.out.write("<table border=1>")
        for fr in frs:
            self.response.out.write("<tr>")
            self.response.out.write("<td>"+ str(fr.forecastFor) +"</td>")
            self.response.out.write("<td>"+ fr.location +"</td>")
            self.response.out.write("<td>"+ str(fr.temp_f) +"</td>")
            self.response.out.write("<td>"+ str(fr.cloudiness) +"</td>")
            self.response.out.write("<td>"+ str(fr.timestamp) +"</td>")
            self.response.out.write("</tr>")
        self.response.out.write("</table>")

class WhatPage(webapp.RequestHandler):
    def get(self):
        template_values = {
            'message': "nothing"
        }

        path = os.path.join(os.path.dirname(__file__),'what.django.html')
        self.response.out.write(template.render(path, template_values))

class ChartPage(webapp.RequestHandler):
    def get(self):
        wr = WeatherRecord()

        wrs = db.GqlQuery("SELECT * FROM WeatherRecord ORDER BY timestamp DESC LIMIT 1000")
        data = ""
        for wr in wrs:

            data += "\t\t[new Date("+str(wr.timestamp.year)+","+str(wr.timestamp.month-1)+","+str(wr.timestamp.day)+","+str(wr.timestamp.hour)+","+str(wr.timestamp.minute)+"), "+str(wr.temp_f)+"],\n"

        template_values = {
            'jsData': data
        }

        path = os.path.join(os.path.dirname(__file__),'chart.django.html')
        self.response.out.write(template.render(path, template_values))


application = webapp.WSGIApplication(
                                     [('/', MainPage),
                                      ('/freezing', FreezingPage),
                                      ('/updateweather', UpdateWeather),
                                      ('/forecast/update', UpdateForecast),
                                      ('/forecast/get', GetForecast),
                                      ('/viewweather', WeatherView),
                                      ('/what', WhatPage),
                                      ('/chart', ChartPage)],
                                     debug=True)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()
